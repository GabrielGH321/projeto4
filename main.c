#include "unity.h"
#include "estatistica.h"

void setUp() {
}

void tearDown() {
}

void teste_media_zero_valores(){
	double v;
	TEST_ASSERT_EQUAL_DOUBLE(media(0,&v), 0.0);
}

void teste_media_um_valor(){
	double v[1] = {11.42};
	TEST_ASSERT_EQUAL_DOUBLE(media(1,v), 11.42);
}

void teste_media_n_negativo(){
	double v;
	TEST_ASSERT_EQUAL_DOUBLE(media(-3,&v), 0.0);
}

void teste_media_inteiro(){
	double v[3] = {3.0,4.0,5.0};
	TEST_ASSERT_EQUAL_DOUBLE(media(3,v), 4.0);
}

void teste_media_double(){
	double v[6] = {3.3,4.4,7.7,32.54,12.87,11.42};
	double result = (3.3+4.4+7.7+32.54+12.87+11.42)/6;
	TEST_ASSERT_EQUAL_DOUBLE(media(6,v), result);
}

void teste_media_valores_negativos(){
	double v[6] = {-3.3,-4.4,-7.7,-32.54,-12.87,-11.42};
	double result = (-3.3+(-4.4)+(-7.7)+(-32.54)+(-12.87)+(-11.42))/6;
	TEST_ASSERT_EQUAL_DOUBLE(media(6,v), result);
}

void teste_media_valores_misturados(){
	double v[6] = {-3.3,4.4,-7.7,32.54,-12.87,11.42};
	double result = (-3.3+4.4+(-7.7)+32.54+(-12.87)+11.42)/6;
	TEST_ASSERT_EQUAL_DOUBLE(media(6,v), result);
}

void teste_desvio_vazio(){
	double v;
	TEST_ASSERT_EQUAL_DOUBLE(desvio(0,&v), 0.0);
}

void teste_desvio_um_valor(){
	double v[1] = {3.14};
	TEST_ASSERT_EQUAL_DOUBLE(desvio(1,v), 0.0);
}

void teste_desvio_n_negativo(){
	double v;
	TEST_ASSERT_EQUAL_DOUBLE(desvio(-3,&v), 0.0);
}

void teste_desvio_tres_numeros(){
	double v[3] = {3.14,5.41,7.65};
	TEST_ASSERT_EQUAL_DOUBLE(desvio(3,v), 2.25501662965);
}

void teste_desvio_dez_numeros(){
	double v[10] = {3.14,5.41,7.65,5,2.76,63.4,0,43.2,87.4,13};
	TEST_ASSERT_EQUAL_DOUBLE(desvio(10,v), 30.7129643998);
}

void teste_desvio_negativos(){
	double v[3] = {-3.14,-5.41,-7.65};
	TEST_ASSERT_EQUAL_DOUBLE(desvio(3,v), 2.25501662965);
}

void teste_desvio_misturados(){
	double v[3] = {3.14,-5.41,7.65};
	TEST_ASSERT_EQUAL_DOUBLE(desvio(3,v), 6.63332747068);
}

int main() {
  UNITY_BEGIN();
	printf("\nTestes media\n");
  RUN_TEST(teste_media_zero_valores);
  RUN_TEST(teste_media_um_valor);
  RUN_TEST(teste_media_n_negativo);
  RUN_TEST(teste_media_inteiro);
  RUN_TEST(teste_media_double);
  RUN_TEST(teste_media_valores_negativos);
  RUN_TEST(teste_media_valores_misturados);
	printf("\nTestes desvio\n");
  RUN_TEST(teste_desvio_vazio);
  RUN_TEST(teste_desvio_um_valor);
  RUN_TEST(teste_desvio_n_negativo);
  RUN_TEST(teste_desvio_tres_numeros);
  RUN_TEST(teste_desvio_dez_numeros);
  RUN_TEST(teste_desvio_negativos);
  RUN_TEST(teste_desvio_misturados);
  return UNITY_END();
}




