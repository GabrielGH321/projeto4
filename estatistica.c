#include "estatistica.h"
#include <math.h>

double media(int n, double *v){
	double total = 0;

	if (n == 0){
		return 0.0;
	}

	for(int i = 0; i < n; i++){
		total = total + v[i];
	}
	total = total/n;
	return total;
}

double desvio(int n, double *v){
	if (n == 0 || n == 1){
		return 0.0;
	}

	double mediaResult = media(n,v);
	double resultado = 0;
	
	for(int i = 0; i < n; i++){
		resultado = resultado + pow((v[i] - mediaResult), 2);
	}
	resultado = resultado/(n-1);
	resultado = sqrt(resultado);
	return resultado;
}





