estatistica: main.o estatistica.o unity.o
	gcc -DUNITY_INCLUDE_DOUBLE main.o estatistica.o unity.o -o main -lm

main.o: main.c estatistica.h unity.h unity_internals.h
	gcc -DUNITY_INCLUDE_DOUBLE -c main.c

estatistica.o: estatistica.c estatistica.h
	gcc -DUNITY_INCLUDE_DOUBLE -c estatistica.c -lm

unity.o: unity.c unity.h unity_internals.h
	gcc -DUNITY_INCLUDE_DOUBLE -c unity.c

clean:
	rm main *.o 
